CREATE TYPE PARTICIPANT_T AS OBJECT (
  empno   NUMBER(4),
  ename   VARCHAR2(20),
  job     VARCHAR2(12),
  mgr     NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  deptno   NUMBER(2)) 


CREATE TYPE MODULE_T  AS OBJECT (
  module_id  NUMBER(4),
  module_name VARCHAR2(20), 
  module_owner REF PARTICIPANT_T, 
  module_start_date DATE, 
  module_duration NUMBER )
  
  

create TYPE MODULETBL_T AS TABLE OF MODULE_T;


CREATE TABLE projects (
  id NUMBER(4),
  name VARCHAR(30),
  owner REF PARTICIPANT_T,
  start_date DATE,
  duration NUMBER(3),
  modules  MODULETBL_T  ) NESTED TABLE modules STORE AS modules_tab ;



CREATE TYPE PHONE_ARRAY IS VARRAY (10) OF varchar2(30)



CREATE TYPE ADDRESS AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
)



CREATE TYPE PERSON AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    ADDRESS
)




CREATE TABLE  employees
( empnumber            INTEGER PRIMARY KEY,
  person_data     REF  person,
  manager         REF  person,
  office_addr          address,
  salary               NUMBER,
  phone_nums           phone_array
)


/*** Clean up ***/
DROP TABLE EMPLOYEES

DROP TABLE PERSONS

DROP TABLE projects

DROP TABLE participants

DROP TYPE PHONE_ARRAY FORCE

DROP TYPE PHONE_TAB FORCE

DROP TYPE PERSON FORCE

DROP TYPE ADDRESS FORCE

DROP TYPE moduletbl_t FORCE

DROP TYPE module_t FORCE

DROP TYPE participant_t FORCE
