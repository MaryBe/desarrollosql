CREATE INDEX wf_cont_reg_id_idx
ON countries(country_name);


CREATE INDEX emps_name_idx
ON employees(first_name, last_name);


SELECT DISTINCT ic.index_name, ic.column_name,
ic.column_position, id.uniqueness
FROM user_indexes id, user_ind_columns ic
WHERE id.table_name = ic.table_name
AND ic.table_name = 'EMPLOYEES';


SELECT *
FROM employees
WHERE UPPER (last_name) = 'KING';


CREATE INDEX upper_last_name_idx
ON employees (UPPER(last_name));


CREATE INDEX emp_hire_year_idx
ON employees (TO_CHAR(hire_date, 'yyyy'));


SELECT first_name, last_name, hire_date
FROM employees
WHERE TO_CHAR(hire_date, 'yyyy') = '2004'

