SELECT first_name, job_title, salary
FROM employees NATURAL JOIN jobs
where min_salary > 5000
order by salary;


SELECT department_name as departamento, street_address as direccion, country_name as pais
from departments join locations using (location_id) join countries using(country_id)
order by department_name;